import java.util.ArrayList;
import java.util.Iterator;


public class Jour {
	private String nom;
	private ArrayList<Creneau> creneaux;
	
	public Jour(String jour){
		nom=jour;
		creneaux=new ArrayList<Creneau>(4);
		creneaux.add(0, new Creneau(this, "8h-10h"));
		creneaux.add(1, new Creneau(this, "10h15-12h15"));
		creneaux.add(2, new Creneau(this, "13h30-15h30"));
		creneaux.add(3, new Creneau(this, "15h45-17h45"));
	}
	
	public Iterator<Creneau> creneauxIterator(){
		return creneaux.iterator();
	}
	
	public String getNom(){
		return nom;
	}

	public Creneau getCreneau(int index) {
		return creneaux.get(index);
	}

	@Override
	public String toString() {
		return "nom=" + nom ;
	}

	public int indexOf(Creneau c) {
		return creneaux.indexOf(c);
	}

}
