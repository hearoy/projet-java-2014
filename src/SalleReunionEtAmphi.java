import java.util.HashMap;


public class SalleReunionEtAmphi extends Salle{

	public SalleReunionEtAmphi(String id, int capacite, HashMap<Creneau, Boolean> disponibilite) {
		super(id, capacite, true, disponibilite);
	}

	public SalleReunionEtAmphi(String id, int capacite) {
		super(id, capacite, true);
	}
	
}
