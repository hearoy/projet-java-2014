import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;


/**Un objet Reservation est associe a un nom unique, celui de la personne qui reserve,
 * et une Map des salles auxquelles on associe une List de Creneau qui sont ceux
 * que la personne a reserves.
 * L'unicite d'un objet est definie par le nom associe.
 */
public class Reservation {
	private String nom;
	private Map<Salle, List<Creneau>> affectations;
	
	/** Constructeur de la Reservation depuis une sauvegarde.
	 * @param nom Nom du membre qui fait la reservation.
	 * @param reservations couples Salle-Creneaux reserves.
	 */
	public Reservation(String nom, Map<Salle, List<Creneau>> reservations) {
		this.nom = nom;
		this.affectations = reservations;
	}
	
	/** Constructeur de la Reservation depuis zero.
	 * @param nom Nom du membre qui fait la reservation.
	 */
	public Reservation(String nom) {
		this.nom = nom;
		affectations = new HashMap<Salle, List<Creneau>>();
	}

	/**
	 * @return Nom du membre qui a fait la reservation.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return Salles et creneaux reserves par le membre.
	 */
	public Map<Salle, List<Creneau>> getReservations() {
		return affectations;
	}
	
	/** Ecrit la partie du flux XML correspondant a une reservation.
	 * @param xWriter curseur juste avant <reservation>
	 * @throws XMLStreamException si probleme d'ecriture XML
	 */
	public void sauvegarder(XMLStreamWriter xWriter) throws XMLStreamException{
		xWriter.writeStartElement("reservation");
		xWriter.writeStartElement("nom");
		xWriter.writeCharacters(nom);
		xWriter.writeEndElement(); // </nom>
		xWriter.writeStartElement("affectations");
		Iterator<Salle> salleIt = affectations.keySet().iterator();
		while( salleIt.hasNext() ) {
			Salle salle=salleIt.next();
			xWriter.writeStartElement("aff");
			xWriter.writeStartElement("salle");
			xWriter.writeStartElement("id");
			xWriter.writeCharacters(salle.getId());
			xWriter.writeEndElement(); // </id>
			xWriter.writeEndElement(); // </salle>
			List<Creneau> listeC=affectations.get(salle);
			Iterator<Creneau> crenIt = listeC.iterator();
			xWriter.writeStartElement("listeCreneaux");
			while( crenIt.hasNext() ) {
				Creneau creneau=crenIt.next();
				xWriter.writeStartElement("creneau");
				xWriter.writeStartElement("jour");
				xWriter.writeStartElement("nom");
				xWriter.writeCharacters(creneau.getNomJour());
				xWriter.writeEndElement(); // </nom>
				xWriter.writeEndElement(); // </jour>
				xWriter.writeStartElement("horaire");
				int index=creneau.getIndiceCreneau();
				xWriter.writeCharacters(Integer.toString(index));
				xWriter.writeEndElement(); // </horaire>
				xWriter.writeEndElement(); // </creneau>
			}
			xWriter.writeEndElement(); // </listeCreneaux>
			xWriter.writeEndElement(); // </aff>
		}
		xWriter.writeEndElement(); // </affectations>
		xWriter.writeEndElement(); // </reservation>
	}



	@Override
	public String toString() {
		String s= "###Reservation###\n"
				+ "\tnom = " + nom + "\n" +
 				"affectations = \n" + affectations
				+ "\n";
		return s;
	}

	/* L'unicite se fait sur le nom uniquement.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	/* L'egalite se fait sur le nom uniquement
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Reservation)) {
			return false;
		}
		Reservation other = (Reservation) obj;
		if (nom == null) {
			if (other.nom != null) {
				return false;
			}
		} else if (!nom.equals(other.nom)) {
			return false;
		}
		return true;
	}

}
