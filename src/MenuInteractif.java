//package projet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MenuInteractif {
	
	private CentraleDeReservation centraledereservations;
	private static Logger log= Logger.getLogger(MenuInteractif.class.getSimpleName());
	
	public MenuInteractif(CentraleDeReservation centraledereservations){
		this.centraledereservations=centraledereservations;
	}
	
	/**Menu interactif qui traite les demandes de l'utilisateur pour les donner a la CentraleDeReservation.
	 * Le menu gere 1 reservation, i.e. pour 1 nom, mais peut prendre en compte plusieurs demandes a la suite pour cette meme personne.
	 * @return la reservation associee a l'utilisateur
	 * @throws AucuneReservationEffectueeException si aucune reservation n'a pu etre faite
	 */
	public Reservation menuReservation() throws AucuneReservationEffectueeException {
		log.setLevel(Level.OFF);
		Map<Salle, List<Creneau>> affectations = new HashMap<Salle,List<Creneau>>(); // on cree le map qui stockera les demandes de salles reservees pour les creneaux choisis
		boolean quitter = false;
		boolean reservationEffectuee= false;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Vous êtes dans le système de reservation de salles du département informatique de la faculté d'Orléans");
		System.out.println("Quel est votre nom?");
		String nom = scanner.next();
		Set<Reservation> reservationsExistantes = centraledereservations.getListReservations();
		Iterator <Reservation> reservIt = reservationsExistantes.iterator();
		while(reservIt.hasNext()){ // on itere dans la liste de reservations stockees dans le fichier
			Reservation r=reservIt.next();
			if( r.getNom().equals(nom)){
				affectations = r.getReservations(); // on recupere la reservation deja effectuee s'il y en a une avec le nom entre
			}
		}
		while(!quitter){
			try{
				System.out.println("Quel type de salle souhaitez-vous? (Tapez cours, TP,TD , ou amphi)");
				String typeDeSalle = "";
				String type=scanner.next();
				log.log(Level.FINER, "type de salle entré: {0}", type);
				log.finer("type.equals(TP)==" + type.equals("TP"));
				if(type.equals("cours")){
					typeDeSalle = "SalleCours";
				}
				else if(type.equals("TP")){
					typeDeSalle = "SalleTP";
				}
				else if(type.equals("TD")){
					typeDeSalle = "SalleTD";
				}
				else if(type.equals("amphi")){
					typeDeSalle = "SalleReunionEtAmphi";
				} // On met la valeur de typeDeSalle egale au nom de la classe afin de faire getClass() dans la methode affecterSalle 
				else{
					throw new ErreurEcritureException();
				}
				System.out.println("Quel est l'effectif demandé?");
				int effectif = scanner.nextInt();
				boolean fixes = false;
				if(typeDeSalle.equals("SalleTP")){
					System.out.println("Souhaitez-vous une salle amenagée avec des ordinateurs fixes ou avec des prises murales pour ordinateurs portables? (tapez fixes ou portables");
					String amenagements=scanner.next();
					if(amenagements.equals("fixes")){
						fixes = true;
					}
					else if(!amenagements.equals("portables")){
						throw new ErreurEcritureException();
					}
				}
				boolean videoproj = true;
				if(typeDeSalle.equals("SalleTD")){
					System.out.println("Avez-vous besoin d'un videoprojecteur?(tapez oui ou non)");
					String demande = scanner.next();
					if(demande.equals("non")){
						videoproj = false;
					}
					else if(!demande.equals("oui")){
						throw new ErreurEcritureException();
					}
				}
				ArrayList<Salle> Listerenvoyee = centraledereservations.chercherSalle(typeDeSalle,effectif,videoproj,fixes);
				System.out.println("Pour quel jour souhaitez-vous la salle?(lundi à vendredi)");
				String jour = scanner.next();
				System.out.println("Pour quel creneau voulez-vous la salle?(Choisir parmis 8h-10h ,10h15-12h15 ,13h30-15h30 ,15h45-17h45 )");
				String creneau = scanner.next();
				Iterator<Salle> SalleIt = Listerenvoyee.iterator(); 
				Salle sallechoisie = null;
				Creneau creneauchoisi = null;

				while(SalleIt.hasNext()){ // On itere sur la liste de salle renvoyee
					Salle salle = SalleIt.next();
					HashMap<Creneau,Boolean> disponibilites = salle.getDisponibilite();
					creneauchoisi = salle.getCreneau(creneau, jour);
					log.log(Level.INFO, "Boolean estDispo = {0}", disponibilites.get(creneauchoisi));
					if(disponibilites.get(creneauchoisi)){// si le creneau est disponible
						salle.put(creneauchoisi, false); // on dit qu'il ne l'est plus
						sallechoisie = salle; //et on indique que cette salle est choisie
						break;
					}
				}
				if(sallechoisie==null){
					throw new PasDeSalleException();
				}
				
				List <Creneau> listeCreneau = new ArrayList<Creneau>(); 
				if(affectations.containsKey(sallechoisie)){ // si le hashmap contient deja une liste de creneaux demandes par cette personne pour cette salle
					listeCreneau=affectations.get(sallechoisie); // on recupere cette liste
				}
				listeCreneau.add(creneauchoisi);
				affectations.put(sallechoisie,listeCreneau); // on ajoute la nouvelle liste de creneaux dans le map avec la salle adequate
				System.out.println("La salle " + sallechoisie.getId() + " de type " + type + " a été reservée au nom de " + nom + " le " + jour + " pour le creneau " + creneau);
				reservationEffectuee = true;
			}	
			catch(PasDeSalleException e){
				System.out.println("Aucune salle qui respecte vos criteres n'a pu etre affectée ");
			}
			catch(ErreurEcritureException e){
				System.out.println("Vous avez mal inscrits vos critères");
			}
			System.out.println("Souhaitez-vous effectuer une autre reservation?(oui/non)");
			if(scanner.next().equals("non")){
				quitter = true;
				scanner.close();
			}
		}
		if(reservationEffectuee){
			return new Reservation(nom,affectations);
		}
		else{
			throw new AucuneReservationEffectueeException();
		}
	}
	
}
