import java.util.HashMap;


public class SalleTP extends Salle{
	protected boolean machinesFixes;
	/** Salle avec videoprojecteur, avec desktops ou pas
	 * @param desktop true si la salle a des machines fixes
	 */
	public SalleTP(String id, int capacite, boolean desktop,
			HashMap<Creneau, Boolean> disponibilite) {
		super(id, capacite, true, disponibilite);
		this.machinesFixes = desktop;
	}

	/** Constructeur depuis sauvegarde.
	 * Salle avec videoprojecteur, avec desktops ou pas
	 * @param desktop true si la salle a des machines fixes
	 */
	public SalleTP(String id, int capacite, boolean desktop) {
		super(id, capacite, true);
		this.machinesFixes = desktop;
		
	}
	public boolean hasMachinesFixes() {
		return machinesFixes;
	}
}