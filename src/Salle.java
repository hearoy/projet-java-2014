//package projet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

public abstract class Salle {
	protected String id;
	protected int capacite;
	protected boolean videoprojecteur;
	protected HashMap <Creneau,Boolean> disponibilite;
	protected static Jour lundi = new Jour("lundi");
	protected static Jour mardi = new Jour("mardi");
	protected static Jour mercredi = new Jour("mercredi");
	protected static Jour jeudi = new Jour("jeudi");
	protected static Jour vendredi = new Jour("vendredi");
	protected static ArrayList<Jour> jours=creerJours();

	/**Constructeur de la salle depuis zero.
	 * @param id identifiant unique
	 * @param capacite Nombre d'eleves maxi.
	 * @param videoprojecteur true si la salle dispose d'un videoprojecteur
	 */
	public Salle(String id, int capacite, boolean videoprojecteur){
		this.id=id;
		this.capacite=capacite;
		this.videoprojecteur=videoprojecteur;
		disponibilite=new HashMap<Creneau, Boolean>();

		
		//Pour chaque clef (creneau de chaque jour) de disponibilite , on initialise la valeur a true
		disponibilite=creerDisponibilites();
	}

	/**
	 * Constructeur de la salle depuis une sauvegarde des disponibilites.
	 * @param id identifiant unique
	 * @param capacite Nombre d'eleves maxi.
	 * @param videoprojecteur true si la salle dispose d'un videoprojecteur
	 * @param disponibilite Tous les creneaux auquels on associe le booleen true si le creneaux est pris.
	 */
	public Salle(String id, int capacite, boolean videoprojecteur,
			HashMap<Creneau, Boolean> disponibilite) {
		super();
		this.id = id;
		this.capacite = capacite;
		this.videoprojecteur = videoprojecteur;
		this.disponibilite = disponibilite;
	}
	

	/** Ecrit la salle dans le flux XML
	 * @param xWriter curseur place ou on doit mettre <salle>
	 * @throws XMLStreamException
	 */
	public void sauvegarder(XMLStreamWriter xWriter) throws XMLStreamException{
		String type=this.getType();
		xWriter.writeStartElement("salle");
		xWriter.writeStartElement("id");
		xWriter.writeCharacters(id);
		xWriter.writeEndElement(); // </id>
		xWriter.writeStartElement("type");
		xWriter.writeCharacters(type);
		xWriter.writeEndElement(); // </type>
		xWriter.writeStartElement("capacite");
		xWriter.writeCharacters(Integer.toString(capacite));
		xWriter.writeEndElement(); // </capacite>
		xWriter.writeStartElement("videoprojecteur");
		xWriter.writeCharacters(Boolean.toString(videoprojecteur));
		xWriter.writeEndElement(); // </videoprojecteur>
		if( this instanceof SalleTP ){
			SalleTP sTP= (SalleTP) this;
			Boolean desktop=sTP.hasMachinesFixes();
			xWriter.writeStartElement("desktop");
			xWriter.writeCharacters(desktop.toString());
			xWriter.writeEndElement(); // </desktop>
		}
		xWriter.writeStartElement("disponibilites");
		Iterator<Creneau> keysIt=disponibilite.keySet().iterator();
		Creneau creneau=null;
		while(keysIt.hasNext()){
			creneau=keysIt.next();
			xWriter.writeStartElement("dispo");
			xWriter.writeStartElement("creneau");
			xWriter.writeStartElement("jour");
			xWriter.writeStartElement("nom");
			xWriter.writeCharacters(creneau.getNomJour());
			xWriter.writeEndElement(); // </nom>
			xWriter.writeEndElement(); // </jour>
			xWriter.writeStartElement("horaire");
			int index=creneau.getIndiceCreneau();
			xWriter.writeCharacters(Integer.toString(index));
			xWriter.writeEndElement(); // </horaire>
			xWriter.writeEndElement(); // </creneau>
			xWriter.writeStartElement("boolean");
			Boolean estDispo=disponibilite.get(creneau);
			xWriter.writeCharacters(estDispo.toString());
			xWriter.writeEndElement(); // </boolean>
			xWriter.writeEndElement(); // </dispo>
		}
		xWriter.writeEndElement(); // </disponibilites>
		xWriter.writeEndElement(); // </salle>
	}
	
	 /** 
	 * Creer l' ArrayList des 5 jours ouvres.
	 */
	private static ArrayList<Jour> creerJours(){
		 ArrayList<Jour> array=new ArrayList<Jour>();
		 array.add(lundi);
		 array.add(mardi);
		 array.add(mercredi);
		 array.add(jeudi);
		 array.add(vendredi);
		 return array;
	 }
	
	/**Créer une map initiale des disponibilites, avec tous les creneaux de la semaine associes
	 * a une disponibilite indiquant que le creneau est libre.
	 * @return une HashMap avec tous les creneaux de la semaine associes a TRUE
	 */
	public static HashMap<Creneau, Boolean> creerDisponibilites(){
		Iterator<Jour> joursIt = jours.iterator(); 
		HashMap<Creneau, Boolean> map=new HashMap<Creneau, Boolean>();
		while(joursIt.hasNext()) { //On itere sur la liste des jours
			Jour j=joursIt.next();
			Iterator<Creneau> creneauxIt = j.creneauxIterator();
			while(creneauxIt.hasNext()){ // On itere sur la liste des creneaux du Jour j
				Creneau c=creneauxIt.next();
				map.put(c, new Boolean(true)); // On met le creneau dans le map avec la valeur true
			}
		}
		return map;
	}

	public String getId() {
		return id;
	}

	public int getCapacite() {
		return capacite;
	}

	public boolean hasVideoprojecteur() {
		return videoprojecteur;
	}
	public HashMap<Creneau, Boolean> getDisponibilite() {
		return disponibilite;
	}
	/**Recupere le nom du type de la salle sans prefixe Salle-
	 * @return String type de la salle sans le prefixe Salle-
	 */
	public String getType(){
		String type=this.getClass().getSimpleName();
		type=type.substring(5); // enleve le prefixe Salle-
//		String minus=type.substring(0, 1).toLowerCase(); // premiere lettre en minuscule
//		String typeCc= minus + type.substring(1); //  en camelCase
		return type;
	}
	
	/** Récupérer l'objet Creneau depuis une description textuelle
	 * @param nomCreneau String du nom du creneau sous forme horaire e.g. 8h-10h
	 * @param nomJour String du nom du jour tout en minuscule
	 * @return l'objet Creneau correspondant aux noms donnes
	 * @throws ErreurEcritureException si nomCreneau ou nomJour ne sont pas reconnus
	 */
	public Creneau getCreneau(String nomCreneau,String nomJour) throws ErreurEcritureException{
	    Iterator<Creneau> cIt = disponibilite.keySet().iterator();
	    while( cIt.hasNext() ) {
	        Creneau c=cIt.next();
	        if( c.getNomJour().equals(nomJour)){
	        	if(c.getNomCreneau().equals(nomCreneau)){
	        		return c;
	        	}
	        }
	    }
	    throw new ErreurEcritureException();
	    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Salle))
			return false;
		Salle other = (Salle) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean put(Creneau key, Boolean value) {
		if(disponibilite.containsKey(key)) {
			disponibilite.put(key, value);
			return disponibilite.put(key, value);
		}	
		else
			return false;
	}

	@Override
	public String toString() {
		return "Salle [id=" + id + ", capacite=" + capacite
				+ ", videoprojecteur=" + videoprojecteur  + ", type="
				+ getClass().getSimpleName() + "]";
	}
	
}
