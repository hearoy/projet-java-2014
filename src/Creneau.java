

/**Chaque objet Creneau est associe a un unique objet Jour
 * et est decrit par une chaine de caracteres qui decrit l'horaire e.g 10h15-12h15.
 * Un creneau peut aussi etre repere et recupere depuis son indice dans la List du jour auquel il est associe
 */
public class Creneau {

	private Jour jour;
	private String creneau;

	public Creneau(Jour j,String c){
		jour=j;
		creneau=c;
	}
	public String getNomJour(){
		return jour.getNom();
	}
	public String getNomCreneau(){
		return creneau;
	}
	@Override
	public String toString() {
		return "Creneau [jour=" + jour + ", creneau=" + creneau + "]";
	}
	
	/** 0->8h-10h 1->10h15-12h15 2->13h30-15h30 3->15h45-17h45
	 * @return indice du Creneau dans la List du Jour associe.
	 */
	public int getIndiceCreneau(){
		java.util.Iterator<Creneau> it=jour.creneauxIterator();
		while(it.hasNext()){
			Creneau test=it.next();
			if(test.equals(this))
				return jour.indexOf(test);
		}
		return -1;
	}
}
