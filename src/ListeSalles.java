import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


public class ListeSalles {
	
	private HashSet<Salle> liste= new HashSet<Salle>();
	private static final Logger log= Logger.getLogger(ListeSalles.class.getName());
	

	/**Constructeur avec une liste venant d'une sauvegarde
	 * @param reader XMLStreamReader dont le curseur est sur <listeSalles
	 * @throws XMLStreamException 
	 */
	public ListeSalles(XMLStreamReader reader) throws XMLStreamException{
		log.setLevel(Level.SEVERE);
		log.log(Level.INFO, "dans ListeSalles(reader)");
		liste=new HashSet<Salle>();
		String tagName;
		int evt=0;
		log.entering(getClass().getName(), "constructor with XMLStreamReader");

		while(reader.hasNext()){ // boucle qui permet de recreer toutes les salles et qui s'arretera a </listeSalles>
			do{ //si le fichier contient autre chose que des balises, cela permet de les passer
				evt=reader.next();
			}while(!reader.isStartElement() && !reader.isEndElement());
			tagName=reader.getLocalName();
			log.fine("tagName : "+tagName);
			if(evt==XMLStreamConstants.END_ELEMENT){
				if("listeSalles".equals(tagName)) // </listeSalles> atteint
					log.exiting(getClass().getName(), "constructor with XMLStreamReader");
					return; // donc on a parcouru toute la section peuplant ListeSalles
			}
			else if(evt==XMLStreamConstants.START_ELEMENT){
				if("salle".equals(tagName)){
					log.info("lancer chargerSalle");
					liste.add(chargerSalle(reader)); // on lance le chargement d'une salle que l'on ajoute a la liste
				}

			}
		}
	}
	
	/**Creer la liste de salles avec un fichier au format suivant :
	 * id:String capacite:int type(cours, TD, TP, amphiEtReunion):String  [videoprojecteur/desktop:boolean]
	 * @param nomFichier chemin du fichier contenant la liste des salles.
	 * @throws FileNotFoundException
	 */
	public ListeSalles(String nomFichier) throws FileNotFoundException{
		Scanner in = new Scanner(new File(nomFichier));
		while (in.hasNextLine()) {
			String id=in.next();
			int capacite=in.nextInt();
			String type =  in.next();
			if(type.equals("cours")){
				liste.add(new SalleCours(id,capacite));
			}
			if(type.equals("TD")){
				boolean videoprojecteur=in.nextBoolean();
				liste.add(new SalleTD(id,capacite,videoprojecteur));
			}
			if(type.equals("TP")){
				boolean desktop=in.nextBoolean();
				liste.add(new SalleTP(id,capacite,desktop));

			}
			if(type.equals("amphiEtReunion")){
				liste.add(new SalleReunionEtAmphi(id,capacite));
			}
		}
		System.out.println(liste.toString());
		in.close();
	}
	
	/**Recree 1 salle depuis sa description dans la sauvegarde, complete avec ses disponibilites
	 * @param reader XMLStreamReader dont le curseur est sur <salle>
	 * @return la salle recree depuis sa description dans la sauvegarde
	 * @throws XMLStreamException si la fin du document est atteinte alors qu'on a demande next()
	 */
	private Salle chargerSalle(XMLStreamReader reader) throws XMLStreamException{
		String id="";
		String typeSalle="";
		int capacite=0;
		boolean videoproj=false;
		boolean desktop=false;
		
		String nomJour="";
		Jour jour=null;
		int indexCreneau=-1;
		Boolean estDispo=null;
		HashMap<Creneau,Boolean> disponibilites=Salle.creerDisponibilites();
		
		String tagName="";

		do{
			reader.next();
			if(reader.isStartElement()){
				log.info("recherche param salle : tagName = "+reader.getLocalName());
				if("disponibilites".equals(reader.getLocalName())) { //boucle qui se charge de recreer les disponibilites de la salle
					do{
						log.info("creation des disponibilites");
						reader.next();
						
						if( reader.isEndElement() && "dispo".equals(reader.getLocalName()) ){ // </dispo> atteint donc indexCreneau, jour et estDispo ont ete trouves au prealable
							disponibilites.put(jour.getCreneau(indexCreneau), estDispo); // on ajoute alors le creneau recompose depuis son indice dans le jour puis le jour et le Boolean associe
						}else if(reader.isStartElement()) {
							tagName=reader.getLocalName();
							log.log(Level.INFO, "dans disponibilites, tagName = {0}", tagName);
							if("nom".equals(tagName)){ // <nom> atteint
								nomJour=reader.getElementText().replaceAll(" ", "");
								Iterator<Jour> it=Salle.jours.iterator();
								while( !nomJour.equals( (jour=it.next()).getNom() ) ){
									continue;
								}
								log.log(Level.INFO, "jour trouvé : {0}", jour.getNom());
							}else if( "horaire".equals(tagName) ){ // <horaire> atteint
								indexCreneau= Integer.parseInt( reader.getElementText().replaceAll(" ", "") );
							}else if( "boolean".equals(tagName)){ // <boolean> atteint
								String bString=reader.getElementText();
								log.log(Level.INFO, "charger Boolean estDispo : {0}", bString);
								estDispo=new Boolean(bString);
							}
						}
					}while( ! reader.isEndElement() || !"disponibilites".equals( reader.getLocalName()) );
				}else{ // partie qui gere les caracteristiques de la salle
					tagName=reader.getLocalName();
					log.fine("autre que disponibilites : "+tagName);
					if("id".equals(tagName)){
						log.info("id trouvé");
						id=reader.getElementText();
						log.info("id = "+id);
					}
					if("videoprojecteur".equals(tagName))
						videoproj=Boolean.parseBoolean( reader.getElementText() );
					if("desktop".equals(tagName))
						desktop=Boolean.parseBoolean( reader.getElementText() );
					if("capacite".equals(tagName))
						capacite=Integer.parseInt( reader.getElementText() );
					if("type".equals(tagName))
						typeSalle=reader.getElementText();
				}
			}
			log.info("isEndElement= "+reader.isEndElement());
			
		}while( !( reader.isEndElement()) || !"salle".equals(reader.getLocalName()) ); // on sort quand </salle> est atteint
		log.info("creation de la salle d'apres parametres trouves\tid= "+id+", type= "+typeSalle+", capa= "+capacite+", videoproj= "+videoproj);
		Salle salle=null; 
		//on lance le constructeur de la salle selon le type trouve
		if("TD".equalsIgnoreCase(typeSalle))
			salle=new SalleTD(id, capacite, videoproj, disponibilites);
		if("TP".equalsIgnoreCase(typeSalle))
			salle=new SalleTP(id, capacite, desktop, disponibilites);
		if("reunionEtAmphi".equalsIgnoreCase(typeSalle))
			salle=new SalleReunionEtAmphi(id, capacite, disponibilites);
		if("cours".equalsIgnoreCase(typeSalle))
			salle=new SalleCours(id, capacite, disponibilites);
		return salle;
	}
	
	/** Ecrit la partie du flux XML correspondant a la liste de salles.
	 * @param xWriter curseur juste avant <listeSalles>
	 * @throws XMLStreamException si probleme d'ecriture XML
	 */
	public void sauvegarder(XMLStreamWriter xWriter) throws XMLStreamException{
		Iterator<Salle> salleIt= liste.iterator();
		Salle salle= null;
		
		xWriter.writeStartElement("listeSalles");
		while(salleIt.hasNext()){
			salle = salleIt.next();
			salle.sauvegarder(xWriter);
		}
		xWriter.writeEndElement(); // </listeSalles>
	}
	
	public Salle getSalleById(String id) throws SalleNotFoundException{
		Iterator<Salle> it=liste.iterator();
		Salle s=null;
		while(it.hasNext()){
			s=it.next();
			if( id.equals(s.getId()) )
				return s;
		}
		throw new SalleNotFoundException();
	}
	public HashSet<Salle> getListeSalle(){
		return liste;
	}
	
	public Iterator<Salle> iterator(){
		return liste.iterator();
	}
	@Override
	public String toString() {
		String s= 
				"%%%%%%%ListeSalles%%%%%%%\n" +
				liste + 
				"\n";
		return s;
	}
}

