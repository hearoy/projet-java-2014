import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


/**Coeur du syteme de reservation.
 * Contient le Set des reservations, la liste des salles et l'objet File de la sauvegarde.
 * 
 * Contient le main : c'est le point de depart pour l'utilisateur qui souhaite reserver une salle.
 */
public class CentraleDeReservation {
	private Set<Reservation> reservations;
	private File sauvegarde;
	private ListeSalles listeSalles;
	private static final Logger log=Logger.getLogger(CentraleDeReservation.class.getName());

	/** Constructeur depuis zero : on definit ici le fichier qui contiendra la sauvegarde et on donne une distribution de salles.
	 * @param sauvegarde File qui contiendra la sauvegarde
	 * @param cheminDistribSalles chemin du fichier qui contient la distribution des salles avec leurs caracteristiques
	 * @throws FileNotFoundException si le chemin du fichier de la distribution de salles n'existe pas ou n'est pas accessible.
	 */
	public CentraleDeReservation(File sauvegarde, String cheminDistribSalles) throws FileNotFoundException{
		reservations= new HashSet<Reservation>();
		listeSalles= new ListeSalles(cheminDistribSalles);
		this.sauvegarde= sauvegarde;
	}
	
	/** Constructeur qui recree les Salles et les Reservations depuis une sauvegarde.
	 * Lecture de la sauvegarde avec l'API StAX, en mode curseur reader.
	 * @param sauv fichier au format xml contenant la sauvegarde bien formatee
	 */
	public CentraleDeReservation(File sauv){
		reservations=new HashSet<Reservation>();
		sauvegarde = sauv;
		XMLInputFactory xmlInFact = XMLInputFactory.newInstance();
		try {
			XMLStreamReader reader = xmlInFact.createXMLStreamReader(new FileInputStream(sauv)); // initialisation du curseur
			String tagName="";
			while(reader.hasNext()){
				int evt=reader.next(); //on recupere l'entier associe qui est caracteristique du type d'evenemet rencontre
				switch(evt){ // test sur reader.next() pour identifier sur quel type d'evenement le curseur s'est arrete
				case XMLStreamConstants.START_ELEMENT: //balise ouvrante <balise>
					tagName = reader.getLocalName(); //on ne peut prendre le nom de la balise que sur un START_ELEMENT ou un END_ELEMENT
					log.fine("tagname = "+tagName);
					if(tagName.equals("listeSalles")){
						log.info("lancer ListeSalles(reader)");
						listeSalles = new ListeSalles(reader); //on lance le constructeur de ListeSalles en lui donnant le reader avec le curseur place sur <listeSalles>
					}
					if("reservations".equals(tagName)){
						Reservation res;
						while( reader.hasNext() ) {
							reader.next();
							while(reader.isStartElement() && "reservation".equals( reader.getLocalName()) ) {
								//boucle a faire autant de fois qu'il y a de reservations
										log.info("chargerReservation");
										res=chargerReservation(reader); // on charge une reservation
										reservations.add(res); // qu'on ajoute au Set<Reservation>
							}
							if( reader.isEndElement() && "reservations".equals(reader.getLocalName()) )
								break;
						}

					}
				}
			}
			reader.close();
		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}
	}
	
	/** Charge une reservation depuis un XMLStreamReader dont le curseur est sur <reservation>.
	 * @param reader reader de la sauvegarde dont le curseur est sur <reservation>
	 * @return 1 Reservation (associee a 1 nom)
	 * @throws XMLStreamException si une erreur de parsing est detectee
	 */
	private Reservation chargerReservation(XMLStreamReader reader) throws XMLStreamException{
		Reservation reservation=null;
		String nom="";
		List<Creneau> listeCreneaux=new ArrayList<Creneau>();
		Map<Salle,List<Creneau>> affectations = new HashMap<Salle, List<Creneau>>();
		String tagName="";
		Salle salle=null;
		String id="";
		String nomJour="";
		Jour jour=null;
		int indexCreneau=-1;
		log.info(reader.getLocalName() + " endElement? " + reader.isEndElement());
		while( reader.hasNext() && ( ! reader.isEndElement() || !"reservation".equals(reader.getLocalName()) ) ) { // on arrete quand on a atteint </reservation>
			//boucle de chargement d'une reservation
			reader.next();
			if(reader.isStartElement()){
				if("nom".equals( reader.getLocalName()) ) // en dehors de <affectations> donc cela correspond au nom de la personne qui a reserve
					nom=reader.getElementText();
				if("affectations".equals(reader.getLocalName())) {
					do{ //boucle de chargement de toutes les affectations
						log.info("boucle charger affectations");
						reader.next();
						
						if( reader.isEndElement() ) {
							if( "aff".equals(reader.getLocalName()) ){ // </aff> atteint donc tous les parametres d'une affectation ont ete trouves prealablement.
							log.info("charger l'affectation : salle= " + salle + ", jour= "
									+ jour + "creneau n° " + indexCreneau);
							//on ajoute la clef<Salle> et la valeur <List<Creneau>> reconstruites a notre map
							affectations.put(salle,listeCreneaux);
							listeCreneaux=new ArrayList<Creneau>(); //on repart d'une liste vide
							}
							else if( "creneau".equals(reader.getLocalName()) ){ // </creneau> atteint donc les parametres du creneau ont ete trouves
								listeCreneaux.add(jour.getCreneau(indexCreneau));
							}
						}else if(reader.isStartElement()) {
							//stockage des parametres d'une affectation
							tagName=reader.getLocalName();
							log.info("element dans affectations : " + tagName);
							if("nom".equals(tagName)){ // <nom> atteint, donne le nom du jour du creneau
								nomJour=reader.getElementText().replaceAll(" ", ""); //on lit le String correspondant au nom du jour
								log.info("nom du jour trouve : " + nomJour);
								Iterator<Jour> it=Salle.jours.iterator();
								while( !nomJour.equals( (jour=it.next()).getNom() ) ){ //recherche de l'objet Jour dont le nom correspond a celui qu'on a trouve
									continue;
								}
								log.info("correspondance d'objet jour= " + jour);
							}else if( "horaire".equals(tagName) ){ // horaire du creneau
								indexCreneau= Integer.parseInt( reader.getElementText().replaceAll(" ", "") ); //creneau repere par son index dans le jour
								log.info("index horaire trouvé : " + indexCreneau);
							}
							else if( "salle".equals(tagName)){ //on est dans <salle>
								do{reader.next();}while(!reader.isStartElement());
								id=reader.getElementText(); // la seule balise possible est <id> qui renseigne l'identifiant de la salle
								log.info("id de la salle : " + id);
								try {
									salle=listeSalles.getSalleById(id); //on recupere l'objet Salle par son id
								} catch (SalleNotFoundException e) {
									e.printStackTrace();
								}
							}
						}
						if(reader.isEndElement())
							log.info( "isEndElement et tagName= " + reader.getLocalName() );
					}while( ! reader.isEndElement() || !"affectations".equals( reader.getLocalName()) ); // on ne sort de la boucle que quand on a atteint </affectations>
					log.info("sorti de la boucle d'affectations");
				}
			}
		};
		log.info( "creation de la reservation pour nom= " + nom );
		reservation=new Reservation(nom, affectations);
		return reservation;
	}
	
	/** Sauvegarde l'etat de la centrale dans le chemin donne
	 * et initialise l'attribut File sauvegarde avec celle creee
	 * @param chemin ou mettre la sauvegarde XML
	 * @return l'instance du fichier de sauvegarde
	 */
	public File sauvegarder(String chemin){
		
		sauvegarde = new File(chemin);
		XMLOutputFactory factory= XMLOutputFactory.newInstance();
		try {
			XMLStreamWriter xWriter= factory.createXMLStreamWriter(new FileOutputStream(sauvegarde));
			xWriter.writeStartDocument();
			xWriter.writeStartElement("sauvegarde");
			listeSalles.sauvegarder(xWriter); // curseur juste avant <listeSalles>
			Iterator<Reservation> resIt = reservations.iterator();
			Reservation resa= null;
			xWriter.writeStartElement("reservations");
			while(resIt.hasNext()) { //boucle sur toutes les reservations
				resa=resIt.next();
				resa.sauvegarder(xWriter); // ecrit 1 reservation
			}
			xWriter.writeEndElement(); // </reservations>
			xWriter.writeEndElement(); // </sauvegarde>
			xWriter.writeEndDocument();
			xWriter.close();
		} catch (FileNotFoundException e) {
			System.out.println("Chemin de fichier non accessible");
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		return sauvegarde;
	}
	
	/** Sauvegarde dans le fichier en attribut et dont le chemin a deja ete defini
	 * @return l'instance du fichier de sauvegarde
	 */
	public File sauvegarder(){
		return sauvegarder(sauvegarde.getAbsolutePath());
	}
	
	/**Cherche les salles candidates a l'affectation selon tous les criteres de l'utilisateur sauf le creneau horaire.
	 * @param typeDeSalle String : "TD", "TP", "cours" ou "amphi"
	 * @param capacite int
	 * @param videoproj true si l'utilisateur veut un videoprojecteur
	 * @param machinesFixes true si l'utilisateur veut des machines fixes. Pris en compte que pour les salles de TP
	 * @return
	 * @throws PasDeSalleException
	 */
	public ArrayList<Salle> chercherSalle(String typeDeSalle,int capacite,boolean videoproj,boolean machinesFixes) throws PasDeSalleException{
		ArrayList<Salle> listerenvoyee = new ArrayList<>();// on cree la liste que l'on va renvoyer
		Iterator<Salle> it=listeSalles.iterator(); //On charge la liste des salles et on itere dessus
		while(it.hasNext()){
			Salle salle=it.next();
			if( typeDeSalle.equals("SalleTP") && salle.getClass().getSimpleName().equals(typeDeSalle) ){ //traitement particulier pour une salle de TP
			SalleTP sTP= (SalleTP) salle; //cast pour atteindre son attribut qui n'est pas dans la classe mere
			 if(sTP.getCapacite()>=capacite){
					if(videoproj==sTP.hasVideoprojecteur()){
						if(machinesFixes==sTP.hasMachinesFixes()){ // On verifie que la salle selectionnee remplisse tous les criteres de l'utilisateur
						listerenvoyee.add(sTP);
						}
						else continue;
					}
					else continue;
				}
			 else	 continue;
			 
			}
			if(typeDeSalle.equals(salle.getClass().getSimpleName())){ // traitement pour tout autre type de salle
				if(salle.getCapacite()>=capacite){
					if(videoproj==salle.hasVideoprojecteur()){// On verifie que la salle selectionnee remplisse tous les criteres de l'utilisateur							
								listerenvoyee.add(salle); // on renvoie la liste de salles
							
					}
				}
			}
		
		}
		if(!listerenvoyee.equals(null)){
			return listerenvoyee;
		}
		throw new PasDeSalleException();
	}

	public Set<Reservation> getListReservations(){
		return reservations;
	}
	
	public ListeSalles getListeSalles(){
		return listeSalles;
	}
	
	/** Routine pour effectuer 1 reservation (1 personne) : 
	 * on recupere une reservation effectuee depuis le menu interactif qu'on ajoute au Set de reservations
	 * puis on sauvegarde. La reservation ajoutee remplace celle associee au meme nom le cas echeant.
	 * @throws AucuneReservationEffectueeException si aucune reservation n'a ete faite a la fin du parcours du menu interactif
	 */
	public void reserver() throws AucuneReservationEffectueeException {
		MenuInteractif menu = new MenuInteractif(this);
		Reservation nlleReserv =null;
		nlleReserv= menu.menuReservation(); //le menu interactif renvoie une Reservation
		reservations.add(nlleReserv);// on l'ajoute aux reservations
		sauvegarder();
	}
	

	@Override
	public String toString() {
		String s="     CentraleDeReservation  \n"+
				" ____________________________\n"+
				"|                            |\n"
				+reservations
				+ "\n|__________________________|\n"+
				"|                          |\n"
				+ listeSalles
				+ "\n|__________________________|\n";
		return s;
	}
	
	
	public static void main(String[] args) {
		log.setLevel(Level.OFF);
		File sauv=new File("sauvegarde"); // la sauvegarde sera contenue dans le fichier eponyme
		CentraleDeReservation centrale = null;
		if( !sauv.exists() ){ // si la sauvegarde n'existe pas encore, on part d'une distribution de salles donnee a part
			while( ! sauv.exists() ) { // boucle qui permet d'assurer l'initialisation du systeme de reservation
				System.out.println("Veuillez rentrer le chemin du fichier contenant la liste de salles :");
				Scanner sc = new Scanner(System.in);
				String cheminDistrib = sc.next();
				try {
					centrale= new CentraleDeReservation(sauv, cheminDistrib);
					centrale.sauvegarder(sauv.getAbsolutePath());
				} catch (FileNotFoundException e) {
					System.out.println("Le chemin donné est incorrect ou le fichier n'existe pas.");
					continue;
				}

			}
		}else { // sinon, on part de la sauvegarde qui contient la liste des salles et les reservations
			centrale = new CentraleDeReservation(sauv);
		}
		
		try {
			centrale.reserver(); // on lance la routine pour effectuer une reservation
		} catch (AucuneReservationEffectueeException e) {
			System.out.println("Aucune réservation n'a été effectuée.");
		}
		
	}

}
