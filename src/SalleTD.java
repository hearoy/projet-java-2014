import java.util.HashMap;


public class SalleTD extends Salle {

	public SalleTD(String id, int capacite, boolean videoprojecteur, HashMap<Creneau, Boolean> disponibilite) {
		super(id, capacite, videoprojecteur, disponibilite);
	}

	public SalleTD(String id, int capacite, boolean videoprojecteur) {
		super(id, capacite, videoprojecteur);
	}

}
