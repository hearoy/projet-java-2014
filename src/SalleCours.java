import java.util.HashMap;


public class SalleCours extends Salle {

	public SalleCours(String id, int capacite, HashMap<Creneau, Boolean> disponibilite) {
		super(id, capacite, true, disponibilite);
	}

	public SalleCours(String id, int capacite) {
		super(id, capacite, true);
	}
	
}
